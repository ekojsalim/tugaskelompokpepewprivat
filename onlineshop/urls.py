from django.urls import path, include
from . import views

app_name = 'onlineshop'

urlpatterns = [
    path('', views.index, name='index'),
]